import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Home from './screens/home/Home';
import Login from './screens/login/Login';
import Navbar from './screens/navbar/Navbar';
import Navbar2 from './screens/navbar2/Navbar2';
import Signup from './screens/signup/Signup';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Signup />
  </React.StrictMode>
);
