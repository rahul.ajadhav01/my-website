import React from "react";
import './Login.css';

const Login = () => {
    return(
        <div className="mainContainer">
            <div className="loginContainer">
                <p className="title">Login To Karveer Darshan</p>
                <input className="input1" type={'email'} placeholder="Email"/>
                <input className="input2" type={'password'} placeholder="Password"/>
                <p onClick={""}>Sign in</p>
            </div>
        </div>
    )
}

export default Login;
