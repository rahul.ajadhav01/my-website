import React, { useState } from "react";
import Navbar from "../navbar/Navbar";
import "./Signup.css";
// import {useNavigate} from "react-router-dom"

const Signup = () => {

    // let navigate = useNavigate()
    const [user, setUser] = useState({
        name: '',
        email: '',
        phone: '',
        password: '',
        cpassword: '',
    })

    let name, value;
    const handelInputs = (e) => {
        console.log(e);
        name = e.target.name;
        value = e.target.value;

        setUser({ ...user, [name]: value })
    }

    const PostData = async (e) => {
        e.preventDefault();

        const { name, email, phone, password, cpassword } = user;

        const res = await fetch("/register", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name, email, phone, password, cpassword
            })
        });
        const data = await res.json();

        if(data.status === 422 || !data) {
            window.alert("Invalid Credentials");
            console.log("Invalid Credentials");
        } else {
            window.alert("successfull register");
            console.log("successfull register");

            // navigate("/login")
        }
    }
    return (
        <>
            <section className="signup">
                <div className="container mt-5">
                    <div className="signup-content">
                        <div className="signup-form">
                            <h2 className="form-title">Sign Up</h2>
                            <form method="POST" className="register-form" id="register-form">
                                <div className="form-group">
                                    <label htmlFor="name">
                                        <i className="zmdi zmdi-account material-icons-name"></i>
                                    </label>
                                    <input type={"text"} name="name" id="name"
                                    value={user.name}
                                    onChange={handelInputs}
                                    placeholder="Your Name"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">
                                        <i className="zmdi zmdi-email material-icons-name"></i>
                                    </label>
                                    <input type={"text"} name="email" id="email"
                                    value={user.email}
                                    onChange={handelInputs}
                                     placeholder="email"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone">
                                        <i className="zmdi zmdi-account material-icons-name"></i>
                                    </label>
                                    <input type={"number"} name="phone" id="phone"
                                    value={user.phone}
                                    onChange={handelInputs}
                                    placeholder="phone"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">
                                        <i className="zmdi zmdi-account material-icons-name"></i>
                                    </label>
                                    <input type={"password"} name="password" id="password"
                                    value={user.password}
                                    onChange={handelInputs}
                                    placeholder="pass"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="cpassword">
                                        <i className="zmdi zmdi-account material-icons-name"></i>
                                    </label>
                                    <input type={"password"} name="cpassword" id="cpassword"
                                    value={user.cpassword}
                                    onChange={handelInputs}
                                    placeholder="Confirm Pass"/>
                                </div>
                            <div className="form-group form-button">
                                <input onClick={PostData} type={"submit"} name="signup" id="signup" className="form-submit" value="register"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Signup;