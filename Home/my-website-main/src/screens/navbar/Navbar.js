import React from "react";
import "./Navbar.css";
import Navbar2 from "../navbar2/Navbar2";

const Navbar = () => {
  return (
    <div>
    <div className="containerNav1">
      <h1 className="heading"><span>K</span>arveer <span>D</span>arshan</h1>
      <ul className="tabsConatiner">
        <li className="tabs">
          <a>📞 1800-234-8220</a>
        </li>
        <li className="tabs">
          <a href="">Home</a>
        </li>
        <li className="tabs">
          <a href="">About</a>
        </li>
        <li className="tabs">
          <a href="">Facilities</a>
        </li>
        <li className="tabs">
          <a href="">Login</a>
        </li>
      </ul>
    </div>
    <Navbar2/>
    </div>
  );
};

export default Navbar;
