import React from "react";
import './Navbar2.css';

const Navbar2 = () => {
    return(
        <div className="container">
           <ul className="tabsContainer">
            <li><a href="#">Honeymoon Packages</a>
            <div className="drop1">
                <div className="flexBox">
                <div className="list1">
                <h1 className="heading1">Indian Destinations</h1>
                <ul className="ul1">
                    <li><a href="#">Kerala</a></li>
                    <li><a href="#">Goa</a></li>
                    <li><a href="#">Mumbai</a></li>
                    <li><a href="#">Delhi</a></li>
                </ul>
                </div> 
                <div className="list1">
                <h1 className="heading1">International Destinations</h1>
                <ul className="ul1">
                    <li><a href="#">Maldives</a></li>
                    <li><a href="#">Dubai</a></li>
                    <li><a href="#">Singapur</a></li>
                    <li><a href="#">Sri Lanka</a></li>
                    <li><a href="#">Bali</a></li>
                    <li><a href="#">Thailand</a></li>
                </ul>
                </div> 
                <img className="image" src={URL="https://plus.unsplash.com/premium_photo-1661776035830-d585626c2bce?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFtaWx5JTIwdHJhdmVsfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"}/>
            </div>
            </div>
            </li>
            <li><a href="#">Family Packages</a>
            <div className="drop1">
                <div className="flexBox">
                <div className="list1">
                <h1 className="heading1">Indian Destinations</h1>
                <ul className="ul1">
                    <li><a href="#">Kerala</a></li>
                    <li><a href="#">Goa</a></li>
                    <li><a href="#">Mumbai</a></li>
                    <li><a href="#">Delhi</a></li>
                </ul>
                </div> 
                <div className="list1">
                <h1 className="heading1">International Destinations</h1>
                <ul className="ul1">
                    <li><a href="#">Maldives</a></li>
                    <li><a href="#">Dubai</a></li>
                    <li><a href="#">Singapur</a></li>
                    <li><a href="#">Sri Lanka</a></li>
                    <li><a href="#">Bali</a></li>
                    <li><a href="#">Thailand</a></li>
                </ul>
                </div> 
                <img className="image" src={URL="https://images.unsplash.com/photo-1506836467174-27f1042aa48c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjd8fGZhbWlseSUyMHRyYXZlbHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60"}/>
            </div>
            </div>
            </li>
            <li><a href="#">Holiday Packages</a>
            <div className="drop1">
                <div className="flexBox">
                <div className="list1">
                <h1 className="heading1">Indian Destinations</h1>
                <ul className="ul1">
                    <li><a href="#">Kerala</a></li>
                    <li><a href="#">Goa</a></li>
                    <li><a href="#">Mumbai</a></li>
                    <li><a href="#">Delhi</a></li>
                </ul>
                </div> 
                <div className="list1">
                <h1 className="heading1">International Destinations</h1>
                <ul className="ul1">
                    <li><a href="#">Maldives</a></li>
                    <li><a href="#">Dubai</a></li>
                    <li><a href="#">Singapur</a></li>
                    <li><a href="#">Sri Lanka</a></li>
                    <li><a href="#">Bali</a></li>
                    <li><a href="#">Thailand</a></li>
                </ul>
                </div> 
                <img className="image" src={URL="https://images.unsplash.com/photo-1528759335187-3b683174c86a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80"}/>
            </div>
            </div>
            </li>
            <li><a className="button" href="">Plan My Holiday</a></li>
            <li><a className="button1" href="">Holiday Themes</a></li>
           </ul>
        </div>
    )
}

export default Navbar2;
