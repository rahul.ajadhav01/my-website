const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

require("../db/connection");
const User = require("../model/userSchema");

router.get("/", (req, res) => {
  res.send(`Hello World router`);
});

router.post("/register", async (req, res) => {
  const { name, email, phone, password, cpassword } = req.body;

  if (!name || !email || !phone || !password || !cpassword) {
    return res.status(422).json({ error: "Plz Filled The Box" });
  }
  try {
    const userExist = await User.findOne({ email: email });

    if (userExist) {
      res.status(422).json({ error: "Email Already Exist" });
    } else if (password != cpassword) {
      res.status(422).json({ error: "Password Does Not Match" });
    } else {
      const user = new User({ name, email, phone, password, cpassword });

      await user.save();

      res.status(201).json({ message: "user register successfully" });
    }
  } catch (err) {
    console.log(err);
  }
});

router.post("/signin", async (req, res) => {
  try {
    let token;

    const { email, password } = req.body;

    if (!email || !password) {
      res.status(400).json({ error: "Please Filled The Data" });
    }

    const userLogin = await User.findOne({ email: email });

    if (userLogin) {
      const isMatch = await bcrypt.compare(password, userLogin.password);

      token = await userLogin.generateAuthToken();

      console.log(token);

      res.cookie("jwtoken", token, {
        expires: new Date(Date.now() + 25892000000),
        httpOnly: true,
      });

      if (!isMatch) {
        res.status(400).json({ error: "Invalid Credintial" });
      } else {
        res.status(200).json({ error: "User Sign In Successfully" });
      }
    } else {
      res.status(400).json({ error: "Invalid Credintial" });
    }
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;