const dotenv = require('dotenv');
const express = require('express');
const mongoose = require('mongoose')
const app = express();

dotenv.config({path: './config.env'})

require('./db/connection');

app.use(express.json());
// const User = require('./model/userSchema')

app.use(require('./router/auth'))

const PORT = process.env.PORT 


const middleware = (req, res, next) => {
    console.log('Hello middleware');
    next();
}

// app.get('/', (req, res) => {
//   res.send(`Hello World`);
// });
app.get('/about',middleware, (req, res) => {
  res.send(`Hello About World`);
});
app.get('/contact', (req, res) => {
  res.cookie("test", 'Rahul');
  res.send(`Hello Contact World`);
});

app.listen(PORT, () => {
  console.log(`Sever is Run on ${PORT}`)
})
//   console.log('Sever is Run on 3000')